from distutils.core import setup

setup(
    name="gitmultisync",
    version="0.0.1",
    description="Attempt concurrent submodule update",
    author="Ian Norton",
    author_email="inorton@gmail.com",
    url="https://gitlab.com/inorton/git-multi-sync",
    packages=["gitmultisync"],
    platforms=["any"],
    license="License :: OSI Approved :: MIT License",
    long_description="Do git submodule update in parallel"
)
