"""
Functions to do some git operations concurrently
"""
from gitcmds import GitCommand


def submodule_update(folder, paths=None):
    """
    Update submodules in parallel
    :param folder:
    :param paths:
    :return:
    """
    # find the submodules
    modules = GitCommand(["git", "submodule"], cwd=folder).run()
    jobs = []
    ended = {}
    paths = []

    for line in modules.splitlines():
        # strip off the first character (" ", "+", "-")
        line = line[1:]

        # strip off everything after the first "("
        if "(" in line:
            line = line.split("(", 1)[0]

        # we should be left with "COMMIT PATH"
        commit, path = line.split(" ", 1)
        paths.append(path)

    if len(paths):
        for path in paths:
            job = GitCommand(["git", "submodule", "update", path])
            print "Start {}".format(job.name)
            jobs.append(job)
            job.begin()

        while len(ended) < len(jobs):
            for job in jobs:
                job.wait(1)
                if job.ended:
                    if job.name not in ended:
                        print "{} complete {}".format(job.name, job.returncode)
                        ended[job.name] = job

        errors = []
        for job in jobs:
            if job.returncode != 0:
                errors.append(job.error)
                print "-- {} error:\n{}".format(job.name, job.output)

        return len(errors)
