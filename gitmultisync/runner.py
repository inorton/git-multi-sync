"""
Command line entry module
"""
import os
import argparse

from gitmultisync import concurrent


def run(args):
    """
    Execute the tool
    :return:
    """
    parser = argparse.ArgumentParser(
        description="Concurrent git operations")
    parser.add_argument("--update-submodules",
                        dest="update_submodules",
                        action="store_true", default=False,
                        help="Update all submodules")

    opts = parser.parse_args(args)

    if opts.update_submodules:
        return concurrent.submodule_update(os.getcwd())

    return 0
