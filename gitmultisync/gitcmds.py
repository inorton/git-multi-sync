"""
Some commands to run git in subshells
"""
import os
import subprocess
import threading


class CommandOperation(object):
    """
    Run a command
    """

    def __init__(self, cmdline, env=None, cwd=None):
        """
        Run cmdline
        :param cmdline: list of command line args
        :param env: environment variables
        :param cwd: working directory
        """
        if env is None:
            env = dict(os.environ)
        if cwd is None:
            cwd = os.getcwd()

        self.cwd = cwd
        self.env = env
        self.cmdline = cmdline
        self.name = " ".join(cmdline)
        self.thread = None
        self.output = None
        self.returncode = None
        self.error = None
        self.ended = False

    def run(self):
        """
        Run the command and return the output
        :return:
        """
        self.begin()
        self.end()
        if self.error is not None:
            raise self.error
        return self.output

    def begin(self):
        """
        Start the command and run it in the background
        :return:
        """
        self.thread = threading.Thread(target=self._start)
        self.thread.start()

    def end(self):
        """
        Wait for the thread to finish
        :return:
        """
        self.thread.join()

    def wait(self, timeout=0):
        """
        Wait at most timeout sec for the command to finish
        :param timeout:
        :return:
        """
        return self.thread.join(timeout)

    def _start(self):
        """
        Run the command
        :return:
        """
        try:
            proc = subprocess.check_output(self.cmdline,
                                           shell=False,
                                           cwd=self.cwd,
                                           env=self.env,
                                           stderr=subprocess.STDOUT)
            self.output = proc
            self.returncode = 0
        except subprocess.CalledProcessError as cpe:
            self.error = cpe
            self.output = cpe.output
            self.returncode = cpe.returncode
        self.ended = True


class GitCommand(CommandOperation):
    """
    Run git
    """
    pass
